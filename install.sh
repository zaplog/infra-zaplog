#!/bin/bash

if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

DIR=$(dirname $(readlink -f $0))
API_DB_USER=zaplog
API_DB_NAME=zaplog
API_DB_PASSWORD=$(openssl rand -base64 32)
MYSQL_ROOT_PASSWORD=$(openssl rand -base64 32)
API_CLIENT_KEY=$(openssl rand -base64 32)

echo "-----------------------"
echo "Installing a new ZAPLOG"
echo "-----------------------"
echo
read -p "Enter your domain name (e.g. example.org)      : " DOMAINS
read -p "Enter an existing email to login to website    : " API_EMAIL_ADMIN
read -p "Enter email for SSL/Certbot (letsencrypt)      : " CERTIFICATE_EMAIL
read -p "Do you want to use subdomain channels? (y/n)   : " USE_SUBDOMAIN_CHANNELS
echo
DOMAINS_LIST=($DOMAINS)
set -- $DOMAINS_LIST
SSL_CERTIFICATE_LOCATION=$1
SERVER_NAME_API="api.$1"
SERVER_NAME_WEB=""
if [ "$USE_SUBDOMAIN_CHANNELS" = y ]; then
echo "You need a domain-name configured in the DNS as a wildcard domain"
CERTBOT_METHOD="--manual --preferred-challenges dns"
WEB_CHANNEL_SUBDOMAIN="true"
SSL_CERTIFICATE_DOMAINS=""
for domain in "${DOMAINS_LIST[@]}"; do
  SSL_CERTIFICATE_DOMAINS="$SSL_CERTIFICATE_DOMAINS-d $domain -d *.$domain "
  SERVER_NAME_WEB="$SERVER_NAME_WEB$domain *.$domain "
done
else
CERTBOT_METHOD="--standalone --preferred-challenges http"
WEB_CHANNEL_SUBDOMAIN="false"
SSL_CERTIFICATE_DOMAINS=""
for domain in "${DOMAINS_LIST[@]}"; do
  SSL_CERTIFICATE_DOMAINS="$SSL_CERTIFICATE_DOMAINS-d $domain -d api.$domain "
  SERVER_NAME_WEB="$SERVER_NAME_WEB$domain api.$domain "
done
fi
echo
echo "Enter the credentials of the SMTP-server for sending mail"
read -p "Enter your SMTP host                           : " API_MAIL_HOST
read -p "Enter your SMTP port                           : " API_MAIL_PORT
read -p "Enter your SMTP user                           : " API_MAIL_LOGIN
read -p "Enter your SMTP password                       : " API_MAIL_PASSWORD
echo 

echo "### Requesting Let's Encrypt certificate for $DOMAINS $SSL_CERTIFICATE_DOMAINS..."
docker run -it --rm --name certbot \
    -p 80:80 \
    -v "$DIR/certbot/data/conf:/etc/letsencrypt" \
    -v "$DIR/certbot/data/www:/var/www/certbot" \
    certbot/certbot \
    certonly \
    $CERTBOT_METHOD \
    --agree-tos --renew-by-default \
    --text --email $CERTIFICATE_EMAIL \
    --rsa-key-size 4096 \
    -v -w /var/www/certbot \
    $SSL_CERTIFICATE_DOMAINS
echo

echo "#nginx
NGINX_SERVER_NAME_API=$SERVER_NAME_API
NGINX_SSL_CERTIFICATE_LOCATION=$SSL_CERTIFICATE_LOCATION
NGINX_SERVER_NAME_WEB=$SERVER_NAME_WEB

#api
API_EMAIL_ADMIN=$API_EMAIL_ADMIN
API_GOTO_URL=https://$SERVER_NAME_API/goto?urlencoded=
API_AUTO_TRANSLATE=0
API_DEEPL_AUTH_KEY=
API_DEEPL_API_URL=
API_DB_NAME=$API_DB_NAME
API_DB_USER=$API_DB_USER
API_DB_PASSWORD=$API_DB_PASSWORD
API_MAIL_HOST=$API_MAIL_HOST
API_MAIL_LOGIN=$API_MAIL_LOGIN
API_MAIL_PASSWORD=$API_MAIL_PASSWORD
API_MAIL_PORT=$API_MAIL_PORT
API_MAIL_SENDER_EMAIL=$API_EMAIL_ADMIN
API_MAIL_SENDER_NAME=$API_EMAIL_ADMIN
API_MAX_MARKDOWN_LENGTH=100000 
API_GENERATE_DIFFS=0
API_DATABASE_QUERY_LOGGING=0
API_CLIENT_KEY=$API_CLIENT_KEY
API_DEFAULT_COPYRIGHT=Some Rights Reserved (CC BY-SA 4.0)

#web
WEB_TITLE=Mindmeld
WEB_IMAGE=/assets/img/logo-mindmeld-outline.svg
WEB_API_URL=https://$SERVER_NAME_API/v1
WEB_DEFAULT_LANGUAGE=nl
WEB_AVAILABLE_LANGUAGES=nl
WEB_GOTO_URL=https://$SERVER_NAME_API/goto?urlencoded=
WEB_CHANNEL_SUBDOMAIN=$WEB_CHANNEL_SUBDOMAIN
WEB_COMMUNITY=true
WEB_NODE_ENV=production
WEB_API_KEY=$API_CLIENT_KEY

#mariadb
MYSQL_TZ=Europe/Amsterdam
MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD" > ".env"

datamodelprefix="DROP SCHEMA IF EXISTS $API_DB_NAME;
CREATE USER '$API_DB_USER'@'%' IDENTIFIED BY '$API_DB_PASSWORD';
CREATE SCHEMA $API_DB_NAME DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON $API_DB_NAME.* TO '$API_DB_USER'@'%';
USE $API_DB_NAME;"
datamodel=`cat datamodel.sql`

echo "$datamodelprefix

$datamodel" > "datamodel.user.sql"

echo "[Spinning up containers]"
docker-compose up -d

echo "[Calling api homepage for initialization]"
wget -O /dev/null https://$SERVER_NAME_API/v1

echo 
echo "---------------------------------------------------------------"
echo "Minimal configuration complete you can now login to your ZAPLOG"
echo "---------------------------------------------------------------"
echo
echo "Goto https://gitlab.com/zaplog/api-zaplog/-/wikis/Zaplog-manual"
echo "For more configuration settings edit .env"
