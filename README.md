# Installing your own Zaplog 

## Using our Docker-container

__(On your own hosting/computer and domain)__

We composed a ready-to-go Zaplog-container that can be installed in 10min. Installation includes configuring the SSL certificates, the database, the web caching and rate-limiting and all other required functionality.

The container has been thoroughly tested in a production environment. A Zaplog-site has stellar performance on a very small footprint. It will run well on a small home-PC behind a fast/stable home-connection, even with 1M pageviews per day. Our own production is at https://zaplog.pro 

### Prerequisites
You need:
- [Docker](https://docs.docker.com/get-docker/)
- Docker-compose 
- [GIT](https://git-scm.com/downloads)

(basically Docker-capable hosting, e.g.: https://digital.com/best-web-hosting/docker/)

Also:
- A domain-name configured as a wildcard domain, e.g.

  | host | value | type  | ttl  |
  |---|---|---|---|
  | example.com | 192.0.2.1 | A | 15 min. |
  | *.example.com | 192.0.2.1 | A | 15 min. |

- An email-address for logging into the Zaplog admin-account (you must be able to access that email)

- The credentials of a SMTP server for sending login and registration mail (https://mailjet.com and https://mailgun.com have limited free accounts), e.g.

    ```
    smtp_host=in-v3.mailjet.com
    smtp_login=<login>
    smtp_password=<password>
    smtp_secure=TLS
    smtp_port=587
    ```

  (Don't forget to whitelist `info@<yourdomain>` on the SMTP-server / mail-account, it wil be used to send registration mail from)

- A logo for your site (preferably a SVG-format image of max. 200x200pixels) uploaded somewhere, e.g. on http://svgur.com like https://svgur.com/i/eA5.svg

- A short title for your site

### HOWTO

Step 1.
```
git clone --depth=1 https://gitlab.com/zaplog/infra-zaplog.git
```

Step 2.
```
cd infra-zaplog
```

Step 3.
```
./install.sh
```

Step 4.
- Enter your domain name 
- Enter desired email to login to website
- Enter the email for SSL/Certbot (letsencrypt)
- Enter your SMTP host
- Enter your SMTP port
- Enter your SMTP user
- Enter your SMTP password

The installation is now available at `<yourdomain>`, you should be seeing an empty website. The login is the email address you entered above.
The system will be sending registration mail from `info@<yourdomain>`. Don't forget to whitelist this address on your SMTP server or account.

Step 5.
- Login to your zaplog and complete the configuration (language, name, logo, etc.) by following [this manual](https://gitlab.com/zaplog/api-zaplog/-/wikis/Zaplog-manual). 

### Manual configuration

You can edit this configuration manually at any time with a text-editor, e.g.:
 
```
vi infra-zaplog/.env
```

### Updating 

TODO...

### Restarting the 'stack'

We never had to do it during operation, but when something gets stuck, it can help to restart the stack:
```
docker-compose up -d
...
